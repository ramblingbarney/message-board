//
//  Avatars.swift
//  Message Board
//
//  Created by The App Experts on 03/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation
import UIKit

class AvatarFiles {
    
    let fileManager = FileManager.default
    
    static let shared = AvatarFiles()
    
    func save(_ image: UIImage, fileName: String) {
        
        let paths = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        
        // 2. Get the URL
        if var location = paths.first {
            
            // 3. Go to the right location/path
            location.appendPathComponent(fileName)
            
            // 4. Convert your object into binary data
            if let imageData = image.pngData() {

                // 5. Save the binary data
                fileManager.createFile(atPath: location.path, contents: imageData, attributes: nil)
            }
        }
    }
    
    func get(_ imageNamed: String) -> UIImage? {
        
        let paths = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        
        // 2. Get the URL
        if var location = paths.first {
            
            // 3. Go to the correct file
            location.appendPathComponent(imageNamed)
            
            // 4. Check if the file exists
            if fileManager.fileExists(atPath: location.path) {
                
                do {
                    // 5. Attempt to get the binary data
                    let someData = try Data(contentsOf: location)
                    
                    // 6. Convert to the correct data type
                    let image = UIImage(data: someData)
                    return image
                } catch {
                    print(error)
                    return nil
                }
                
                // some functions have supplied methods for resource retrival [curl], this silently fails e.g. try??
                //return UIImage(contentsOfFile: location.path)
            }
        }
        return nil
    }
}
