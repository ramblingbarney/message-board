//
//  UserViewController.swift
//  Message Board
//
//  Created by The App Experts on 04/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit

protocol DataReloadTableViewPostDelegate: class{
    func reloadPostTable()
}

class UserViewController: UIViewController {
    
    let cellId = "asfjowieurilsjflksjdflkjdsf2923983298dsfljwlerj34"
    var urlComponents = URLComponents()
    var user_id = ""
    var full_name = ""
    var user_name = ""
    var email_address = ""
    var postal_address_street = ""
    var postal_address_suite = ""
    var postal_address_city = ""
    var postal_address_zipcode = ""
    var telephone = ""
    var website_url = ""
    var iconImage: UIImage?
    var iconView: UIImageView!
    var lbl1:UILabel!
    var lbl2:UILabel!
    var lbl3:UILabel!
    var lbl4:UILabel!
    var lbl5:UILabel! //Address
    var lbl6:UILabel! //Address
    var lbl7:UILabel! //Address
    var lbl8:UILabel! //Address
    var postsTable:UITableView!
    var model: ModelPosts!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.title = user_name
        iconImage = AvatarFiles.shared.get(user_name + ".png") ?? UIImage(imageLiteralResourceName: "placeholder-man")
        model = ModelPosts()
        model.delegate = self
        setupUI()
        constraints()
        fetchPostData()
        
        postsTable.dataSource = self
        postsTable.delegate = self
        postsTable.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        postsTable.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    private func setupUI(){
        
        iconView = UIImageView(image: iconImage)
        iconView.frame = CGRect(x: 50, y: 50, width: 100, height: 100)
        self.view.addSubview(iconView)
        
        //Label 1
        lbl1 = UILabel(frame: CGRect(x: 10, y: 50, width: 230, height: 21))
        lbl1.text = full_name
        lbl1.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl1)
        
        //Label 2
        lbl2 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl2.text = email_address
        lbl2.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl2)
        
        //Label 3
        lbl3 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl3.text = telephone
        lbl3.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl3)
        
        //Label 4
        lbl4 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl4.text = website_url
        lbl4.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl4)
        
        //Label 5 Address Line
        lbl5 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl5.text = postal_address_street
        lbl5.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl5)
        
        //Label 6 Address Line
        lbl6 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl6.text = postal_address_suite
        lbl6.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl6)
        
        //Label 7 Address Line
        lbl7 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl7.text = postal_address_city
        lbl7.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl7)
        
        //Label 8 Address Line
        lbl8 = UILabel(frame: CGRect(x: 10, y: 150, width: 230, height: 21))
        lbl8.text = postal_address_zipcode
        lbl8.myLabel()//Call this function from extension to all your labels
        view.addSubview(lbl8)
        
        postsTable = UITableView(frame: .zero, style: .plain)
        view.addSubview(postsTable)
    }
    
    private func constraints(){
        
        if WhatIsDevice.what.portrait {
            
            let margins = view.layoutMarginsGuide
            iconView.translatesAutoresizingMaskIntoConstraints = false
            iconView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0.0).isActive = true
            iconView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 5.0).isActive = true
            view.addSubview(iconView)
            
            lbl1.translatesAutoresizingMaskIntoConstraints = false
            lbl1.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl1.topAnchor.constraint(equalTo: margins.topAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl1)
            
            lbl2.translatesAutoresizingMaskIntoConstraints = false
            lbl2.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl2.topAnchor.constraint(equalTo: lbl1.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl2)
            
            lbl3.translatesAutoresizingMaskIntoConstraints = false
            lbl3.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl3.topAnchor.constraint(equalTo: lbl2.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl3)
            
            lbl4.translatesAutoresizingMaskIntoConstraints = false
            lbl4.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl4.topAnchor.constraint(equalTo: lbl3.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl4)
            
            lbl5.translatesAutoresizingMaskIntoConstraints = false
            lbl5.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl5.topAnchor.constraint(equalTo: lbl4.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl5)
            
            lbl6.translatesAutoresizingMaskIntoConstraints = false
            lbl6.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl6.topAnchor.constraint(equalTo: lbl5.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl6)
            
            lbl7.translatesAutoresizingMaskIntoConstraints = false
            lbl7.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl7.topAnchor.constraint(equalTo: lbl6.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl7)
            
            lbl8.translatesAutoresizingMaskIntoConstraints = false
            lbl8.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl8.topAnchor.constraint(equalTo: lbl7.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl8)
            
            postsTable.translatesAutoresizingMaskIntoConstraints = false
            postsTable.topAnchor.constraint(equalTo:lbl8.bottomAnchor, constant: 20.0).isActive = true
            postsTable.leadingAnchor.constraint(equalTo:margins.leadingAnchor).isActive = true
            postsTable.trailingAnchor.constraint(equalTo:margins.trailingAnchor).isActive = true
            postsTable.bottomAnchor.constraint(equalTo:margins.bottomAnchor, constant: -20.0).isActive = true
            view.addSubview(postsTable)
        } else {
            
            
            // Left Aligned
            
            let margins = view.layoutMarginsGuide
            iconView.translatesAutoresizingMaskIntoConstraints = false
            iconView.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 0.0).isActive = true
            iconView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 5.0).isActive = true
            view.addSubview(iconView)
            
            // Right Aligned
            lbl1.translatesAutoresizingMaskIntoConstraints = false
            lbl1.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl1.topAnchor.constraint(equalTo: margins.topAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl1)
            
            lbl2.translatesAutoresizingMaskIntoConstraints = false
            lbl2.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl2.topAnchor.constraint(equalTo: lbl1.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl2)
            
            lbl3.translatesAutoresizingMaskIntoConstraints = false
            lbl3.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl3.topAnchor.constraint(equalTo: lbl2.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl3)
            
            lbl4.translatesAutoresizingMaskIntoConstraints = false
            lbl4.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -5.0).isActive = true
            lbl4.topAnchor.constraint(equalTo: lbl3.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl4)
            
            // Middle Aligned
            
            lbl5.translatesAutoresizingMaskIntoConstraints = false
            lbl5.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 5.0).isActive = true
            lbl5.topAnchor.constraint(equalTo: margins.topAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl5)
            
            lbl6.translatesAutoresizingMaskIntoConstraints = false
            lbl6.leadingAnchor.constraint(equalTo: lbl5.leadingAnchor, constant: 0.0).isActive = true
            lbl6.topAnchor.constraint(equalTo: lbl5.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl6)
            
            lbl7.translatesAutoresizingMaskIntoConstraints = false
            lbl7.leadingAnchor.constraint(equalTo: lbl6.leadingAnchor, constant: 0.0).isActive = true
            lbl7.topAnchor.constraint(equalTo: lbl6.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl7)
            
            lbl8.translatesAutoresizingMaskIntoConstraints = false
            lbl8.leadingAnchor.constraint(equalTo: lbl7.leadingAnchor, constant: 0.0).isActive = true
            lbl8.topAnchor.constraint(equalTo: lbl7.bottomAnchor, constant: 1.0).isActive = true
            view.addSubview(lbl8)
            
            // Page Width
            
            postsTable.translatesAutoresizingMaskIntoConstraints = false
            postsTable.topAnchor.constraint(equalTo:iconView.bottomAnchor, constant: 20.0).isActive = true
            postsTable.leadingAnchor.constraint(equalTo:margins.leadingAnchor).isActive = true
            postsTable.trailingAnchor.constraint(equalTo:margins.trailingAnchor).isActive = true
            postsTable.bottomAnchor.constraint(equalTo:margins.bottomAnchor, constant: -20.0).isActive = true
            view.addSubview(postsTable)
        }
    }
    
    @objc func fetchPostData() -> Void {
        
        urlComponents.scheme = TYPICODE.scheme
        urlComponents.host = TYPICODE.host
        urlComponents.path = ENDPOINTS.posts
        urlComponents.queryItems = [
            URLQueryItem(name: "userId", value: user_id),
        ]
        
        guard let url:URL = urlComponents.url else { return }
        
        NetworkManager.shared.fetchPosts(url: url, completion: { value in
            
            guard let posts:[Post] = value else { return }
            self.model.savePosts(arrayPosts: posts)
            
            if self.model.posts.count > 0 {
                
                self.fetchCommentData()
            }
        })
    }
    
    @objc func fetchCommentData() -> Void {
        
        urlComponents.scheme = TYPICODE.scheme
        urlComponents.host = TYPICODE.host
        urlComponents.path = ENDPOINTS.comments
        
        guard let url:URL = urlComponents.url else { return }
        
        NetworkManager.shared.fetchComments(url: url, completion: { value in
            
            guard let comments:[Comment] = value else { return }
            self.model.saveComments(arrayComments: comments)
        })
    }
}

extension UILabel {
    func myLabel() {
        textAlignment = .right
        textColor = .black
        font = UIFont.systemFont(ofSize: 17)
        numberOfLines = 0
        lineBreakMode = .byCharWrapping
        sizeToFit()
    }
}

extension UserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = model.posts[indexPath.row].post_title
        cell.detailTextLabel?.text = model.counts[model.posts[indexPath.row].post_id]
        DispatchQueue.main.async {
            self.postsTable.reloadData()
        }
        return cell
    }
}

extension UserViewController: DataReloadTableViewPostDelegate{
    
    func reloadPostTable(){
        
        DispatchQueue.main.async {
            self.postsTable.reloadData()
        }
    }
}
