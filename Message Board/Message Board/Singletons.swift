//
//  Singletons.swift
//  Message Board
//
//  Created by The App Experts on 04/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation
import UIKit

class WhatIsDevice {
    
    static let what = WhatIsDevice()
    
    var landscape: Bool { get { UIDevice.current.orientation.isLandscape }}
    var portrait: Bool { get { UIDevice.current.orientation.isPortrait }}
}
