//
//  ViewController.swift
//  Message Board
//
//  Created by The App Experts on 01/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit

protocol DataReloadTableViewDelegate: class{
    func reloadNetworkTable()
}

class AllUsersViewController: UITableViewController {
    
    let cellId = "asfjowieurilsjflksjdflkjdsf292398329834"
    var model: ModelUsers!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "All Users"
        view.backgroundColor = .white
        model = ModelUsers()
        model.delegate = self
        setupUI()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        fetchUserData()
    }
    
    
    private func setupUI(){
        
        tableView.tintColor = .blue
        tableView.separatorColor = .blue
    }
    
    @objc func fetchUserData() -> Void {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = TYPICODE.scheme
        urlComponents.host = TYPICODE.host
        urlComponents.path = ENDPOINTS.users
        
        guard let url:URL = urlComponents.url else { return }
        
        NetworkManager.shared.fetchUsers(url: url, completion: { value in
            
            guard let users:[User] = value else { return }
            self.model.saveUser(arrayUsers: users)
        })
    }
}

extension AllUsersViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.countUsers
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = model.users[indexPath.row].user_name
        
        if AvatarFiles.shared.get(model.users[indexPath.row].user_name + ".png") != nil {
            
            cell.imageView?.image = AvatarFiles.shared.get(model.users[indexPath.row].user_name + ".png")
        } else  {
            
            var urlComponents = URLComponents()
            urlComponents.scheme = AVATARS.scheme
            urlComponents.host = AVATARS.host
            urlComponents.path = ENDPOINTS.avatars + model.users[indexPath.row].user_name + ".png"
            
            guard let url:URL = urlComponents.url else { return cell }
            
            NetworkManager.shared.fetchImage(imageURL: url, completion: { image in
                
                guard let imageToSave = image else { return }
                AvatarFiles.shared.save(imageToSave, fileName: self.model.users[indexPath.row].user_name + ".png")
            })
            
            cell.imageView?.image = AvatarFiles.shared.get(model.users[indexPath.row].user_name + ".png")
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UserViewController()
        vc.user_id = String(model.users[indexPath[1]].user_id)
        vc.user_name = model.users[indexPath[1]].user_name
        vc.full_name = model.users[indexPath[1]].full_name
        vc.user_name = model.users[indexPath[1]].user_name
        vc.email_address = model.users[indexPath[1]].email_address
        vc.postal_address_street = model.users[indexPath[1]].postal_address.street
        vc.postal_address_suite = model.users[indexPath[1]].postal_address.suite
        vc.postal_address_city = model.users[indexPath[1]].postal_address.city
        vc.postal_address_zipcode = model.users[indexPath[1]].postal_address.zipcode
        vc.telephone = model.users[indexPath[1]].telephone
        vc.website_url = model.users[indexPath[1]].website_url
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension AllUsersViewController: DataReloadTableViewDelegate{
    
    func reloadNetworkTable(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

