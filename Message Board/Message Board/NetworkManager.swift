//
//  NetworkManager.swift
//  Message Board
//
//  Created by The App Experts on 02/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Network Manager
class NetworkManager {
    
    // The session to use to download the data
    private let session: URLSession
    
    // Create the manager with a given session configuration
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
    }
    
    static let shared = NetworkManager()
    
    // A private function to fetch data from a URL
    private func fetchData(from url: URL, completion: @escaping (Data?) -> Void) {
        
        let request = URLRequest(url: url)
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error)
                completion(nil)
            } else if let data = data {
                completion(data)
            }
        }
        
        dataTask.resume()
        
    }
}

extension NetworkManager {
    
    func fetchUsers(url: URL,  completion: @escaping ([User]?) -> Void) {
        
        // Fetch the data
        fetchData(from: url) { data in
            
            if let data = data {
                
                // Parse the response
                do {
                    let value = try JSONDecoder().decode([User].self, from: data)
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
}

extension NetworkManager {
    
    func fetchPosts(url: URL,  completion: @escaping ([Post]?) -> Void) {
        
        // Fetch the data
        fetchData(from: url) { data in
            
            if let data = data {
                // turn the Data to String
                guard let dataString = String(data: data, encoding: .utf8) else { return completion(nil) }
                
                let safeData = dataString.replacingOccurrences(of: "\n", with: "")
                
                // turn the String to Data
                guard let fixedData = safeData.data(using: .utf8) else { return completion(nil) }
                
                // Parse the response
                do {
                    let value = try JSONDecoder().decode([Post].self, from: fixedData)
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
}

extension NetworkManager {
    
    func fetchComments(url: URL,  completion: @escaping ([Comment]?) -> Void) {
        
        // Fetch the data
        fetchData(from: url) { data in
            
            if let data = data {
                // turn the Data to String
                guard let dataString = String(data: data, encoding: .utf8) else { return completion(nil) }
                
                let safeData = dataString.replacingOccurrences(of: "\n", with: "")
                
                // turn the String to Data
                guard let fixedData = safeData.data(using: .utf8) else { return completion(nil) }
                
                // Parse the response
                do {
                    let value = try JSONDecoder().decode([Comment].self, from: fixedData)
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
}

extension NetworkManager {
    
    // A function to fetch the image of the article
    func fetchImage(imageURL: URL, completion: @escaping (UIImage?) -> Void) {
        
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self = self else {
                return
            }
            
            // Fetch the data
            self.fetchData(from: imageURL) { data in
                
                if let data = data {
                    // Call the completion with the data
                    completion(UIImage(data: data))
                } else {
                    completion(nil)
                }
            }
        }
    }
}
