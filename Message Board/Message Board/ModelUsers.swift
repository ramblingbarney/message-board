//
//  UsersModel.swift
//  Message Board
//
//  Created by The App Experts on 02/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation

struct User: Codable {
    
    let user_id: Int
    let full_name: String
    let user_name: String
    let email_address: String
    let postal_address: AddressDetails
    let telephone: String
    let website_url: String
    
    private enum CodingKeys: String, CodingKey {
        case full_name = "name"
        case telephone = "phone"
        case user_id = "id"
        case website_url = "website"
        case email_address = "email"
        case user_name = "username"
        case postal_address = "address"
    }
}

struct AddressDetails: Codable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
}

class ModelUsers {
    
    weak var delegate: DataReloadTableViewDelegate?
    var users:[User]
    var countUsers:Int {
        get { return users.count }
    }
    
    init(){
        users = []
    }
    
    func saveUser(arrayUsers: [User]){
        
        users = arrayUsers
        getAvatars()
        delegate?.reloadNetworkTable()
    }
    
    func getAvatars(){
        
        var urlComponents = URLComponents()
        
        for user in users{
            
            urlComponents.scheme = AVATARS.scheme
            urlComponents.host = AVATARS.host
            urlComponents.path = ENDPOINTS.avatars + user.user_name + ".png"
            
            guard let url:URL = urlComponents.url else { return }
            
            NetworkManager.shared.fetchImage(imageURL: url, completion: { image in
                
                guard let imageToSave = image else { return }
                AvatarFiles.shared.save(imageToSave, fileName: user.user_name + ".png")
            })
        }
    }
}
