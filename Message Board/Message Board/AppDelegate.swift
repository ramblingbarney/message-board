//
//  AppDelegate.swift
//  Message Board
//
//  Created by The App Experts on 01/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let allUsersViewController = AllUsersViewController()
        let navigationController = UINavigationController(rootViewController: allUsersViewController)
        
        // Mark: Navigation Bar
        navigationController.navigationBar.barTintColor = UIColor.blue
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }
    
}
