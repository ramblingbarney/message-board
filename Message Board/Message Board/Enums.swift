//
//  enums.swift
//  Message Board
//
//  Created by The App Experts on 02/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation

enum ENDPOINTS {
    
    static let users = "/users"
    static let posts = "/posts"
    static let comments = "/comments"
    static let avatars = "/avatars/100/"
}

enum TYPICODE {
    
    static let scheme = "http"
    static let host = "jsonplaceholder.typicode.com"
}

enum AVATARS {
    
    static let scheme = "http"
    static let host = "api.adorable.io"
}
