//
//  PostsModel.swift
//  Message Board
//
//  Created by The App Experts on 03/11/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import Foundation

struct Post: Codable {
    let post_body: String
    let post_id: Int
    let post_title: String
    let user_id: Int
    
    private enum CodingKeys: String, CodingKey {
        case post_body = "body"
        case post_id = "id"
        case post_title = "title"
        case user_id = "userId"
    }
}

struct Comment: Codable {
    let comment_body: String
    let comment_id: Int
    let comment_name: String
    let comment_email: String
    let post_id: Int
    
    private enum CodingKeys: String, CodingKey {
        case comment_body = "body"
        case comment_id = "id"
        case comment_name = "name"
        case comment_email = "email"
        case post_id = "postId"
    }
}

class ModelPosts {
    
    weak var delegate: DataReloadTableViewPostDelegate?
    var posts:[Post]
    var comments:[Comment]
    var counts: [Int: String]
    
    init(){
        
        posts = []
        comments = []
        counts = [:]
    }
    
    func savePosts(arrayPosts: [Post]){
        
        posts = arrayPosts
        delegate?.reloadPostTable()
    }
    
    func saveComments(arrayComments: [Comment]){
        
        comments = arrayComments
        countComments()
    }
    
    private func countComments() {
        
        for element in posts {
            
            var count = 0
            
            _ = comments.map( { if $0.post_id == element.post_id {
                
                count += 1
                }})
            counts[element.post_id] = String(count)
        }
    }
}
